Keybase client docker container
===============================

Docker container with command line keybase client.


Usage
-----

By default, the container starts the keybase background service.

    docker run -d --name keybase beli/keybase

Or you can provide a persistent config directory from host, mounted on
`/config` inside the container

    docker run -d --name keybase -v ~/keybase-config:/config beli/keybase

Then you can run the command line client in the container

    docker exec -ti keybase keybase login

(first parameter is the name of the *keybase* container and then the *keybase*
command with *login* argument)

Your session will remain active thanks to the background service running in
the container. The service can be stopped by stopping and optionally removing
the container along with the auto-created persistent volumes:

    docker stop keybase
    docker rm -v keybase

For convenience, I add these functions/aliases to my `~/.bashrc`:

    function start_keybase {
      docker rm -v keybase
      docker run -d --name keybase -v ~/Private/keybase-config:/config beli/keybase
    }
    function stop_keybase {
      docker stop keybase
      docker rm -v keybase
    }
    alias keybase='docker exec -ti keybase keybase'

Then at the start of a session I type `start_keybase` and I can use the `keybase`
command as if it was installed locally, typing `stop_keybase` when finished.


Volumes
-------

The container has two volumes defined:

`/config` - keybase's config directory (linked to `~/.config/keybase`)

`/gnupg` - your GnuPG directory with keyrings, etc. if you would like to use
your GnuPG keys with keybase.

Processes in the container run under UID 1000, GID 1000, so make sure the
volumes have owner and permissions set accordingly.

